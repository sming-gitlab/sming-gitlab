# Samantha Ming's README

Hello, here's a glance of how I work 👋  I hope by being transparent, it will help others understand better what it might be like working with me; and make it easier to build a more honest and authentic working relationship. Thank you for reading this and I look forward to collaborating with you 🤝 

## ☕️ Coffee Chats 

I love meeting new people and having coffee chats. I prefer grouping my calls on one day, **Meeting Wednesdays**. If possible, please send an invite on that day. However I understand that this may not work with your schedule; in those cases, please send me an invite at your convenience 😀

❣️ Please note social meetings can be quite mentally draining for me and it takes me a long time to recover afterwards. To help me better preserve my mental energy, I only have the capacity to have a maximum of **2** coffee chats per week. As well, I **kindly decline recurring** coffee chats. These boundaries ensure I can prioritize my energy on work-related tasks and balance my mental health 🧘

## 🗓 Group Meetings 

I prefer 1:1 meetings over group meetings. I get extremely anxious in a group setting. So I only attend the ones when absolutely required. Otherwise, I prefer attending them in an async manner.

> [GitLab Handbook](https://about.gitlab.com/company/culture/all-remote/meetings/): As an all-remote company, we do not look to a meeting by default; when they are necessary, we strive to make in-person attendance optional by enabling asynchronous contribution. This works because of our values, which leads GitLab to hire individuals who enjoy being a manager of one, a point detailed in our Efficiency value. Team members are empowered to decide whether a sync meeting is the best use of their time, and set boundaries when needed.

## 🤝 Prefer Collaborative Work

I love collaborating synchronously 🤝 I will gladly jump on ad-hoc meetings to hash out problems or brainstorm ideas; ping me **anytime**. I truly believe solutions through collaborations are always better. Also, it's less lonely and way more fun! It is my preferred working style 👍

Did you know there was a [study](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3291107/) that indicated the perception of a hard task is significantly lowered when the participant is paired with someone versus going at it alone 🤯

I'm a huge fan of collaboration. I love pairing up with other team members to deliver [results](https://about.gitlab.com/handbook/values/#results) for our GitLab community 🙌
